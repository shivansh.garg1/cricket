import java.util.Random;

public class Bowler {
    private final BowlerType bowlerType;

    public Bowler( BowlerType bowlerType) {
        this.bowlerType = bowlerType;
    }

    public int bowl() {
        Random rand = new Random();
        return rand.nextInt(7);
    }

    public boolean isPartTimeBowler() {
        return bowlerType == BowlerType.PartTime;
    }
}
