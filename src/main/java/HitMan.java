import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HitMan implements Batsman{
    @Override
    public int bat() {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(4);
        list.add(6);

        Random rand = new Random();
        return list.get(rand.nextInt(list.size()));
    }
}
