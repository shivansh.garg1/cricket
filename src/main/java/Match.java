public class Match {

    Batsman batsman;
    Bowler bowler;
    private final int target;
    private final int overs;

    private int score;

    public Match(Batsman batsman, Bowler bowler, int target, int overs) {
        this.batsman = batsman;
        this.bowler = bowler;
        this.target = target;
        this.overs = overs;
    }

    public void play() {
        score = 0;
        int totalBalls = overs * 6;
        for (int ball = 0; ball < totalBalls; ball++) {
            if(!wonMatch()) {
                if (isBatsmanOut()) return;
            }
        }
    }

    private boolean isBatsmanOut() {
        if(batsman.bat() == bowler.bowl()) {
            return !bowler.isPartTimeBowler();
        } else {
            score += batsman.bat();
        }
        return false;
    }


    public boolean wonMatch() {
        return target <= score;
    }
}
