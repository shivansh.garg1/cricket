import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DefensiveBatsman implements Batsman{

    @Override
    public int bat() {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);

        Random rand = new Random();
        return list.get(rand.nextInt(list.size()));
    }
}
