import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DefensiveBatsmanTest {

    @Test
    void shouldReturnFalseIfDefensiveBatsmanScoresOtherThanZeroOrOneOrTwoOrThree() {
        Batsman defensiveBatsman = new DefensiveBatsman();

        List<Integer> list = new ArrayList<>();
        list.add(4);
        list.add(5);
        list.add(6);
        int score = defensiveBatsman.bat();

        assertFalse(list.contains(score));
    }

}