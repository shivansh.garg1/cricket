import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HitManTest {

    @Test
    void shouldReturnFalseIfHitManScoresOtherThanOnlyZeroOrFourOrSix() {
        Batsman hitMan = new HitMan();

        List<Integer> list = Arrays.asList(1,2,3,5);
        int score = hitMan.bat();

        assertFalse(list.contains(score));
    }

}