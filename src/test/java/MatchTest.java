import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MatchTest {

    @Test
    void shouldReturnTrueIfBatsmanChasesTheTargetSuccessfully() {
        int target = 23;
        int overs = 1;
        Batsman batsman = mock(NormalBatsman.class);
        Bowler bowler = mock(Bowler.class);
        Match match = new Match(batsman, bowler, target, overs);
        when(batsman.bat()).thenReturn(5);
        when(bowler.bowl()).thenReturn(2);

        match.play();

        assertTrue(match.wonMatch());
    }

    @Test
    void shouldReturnFalseIfBatsmanGetsOutBeforeChasingTheTarget() {
        int target = 48;
        int overs = 2;
        Batsman batsman = mock(HitMan.class);
        Bowler bowler = mock(Bowler.class);
        Match match = new Match(batsman, bowler, target, overs);
        when(batsman.bat()).thenReturn(6);
        when(bowler.bowl()).thenReturn(6);

        match.play();

        assertFalse(match.wonMatch());
    }

    @Test
    void shouldReturnFalseIfBatsmanDoesNotChaseTargetSuccessfully() {
        int target = 48;
        int overs = 2;
        Batsman batsman = mock(HitMan.class);
        Bowler bowler = mock(Bowler.class);
        Match match = new Match(batsman, bowler, target, overs);
        when(batsman.bat()).thenReturn(2);
        when(bowler.bowl()).thenReturn(6);

        match.play();

        assertFalse(match.wonMatch());
    }

    @Test
    void shouldReturnTrueIfBatsmanChasesTargetSuccessfullyWithPartTimeBowler() {
        int target = 48;
        int overs = 2;
        Batsman batsman = mock(NormalBatsman.class);
        Bowler bowler = mock(Bowler.class);
        Match match = new Match(batsman, bowler, target, overs);
        when(batsman.bat()).thenReturn(2);
        when(bowler.bowl()).thenReturn(6);

        match.play();

        assertFalse(match.wonMatch());
    }



}